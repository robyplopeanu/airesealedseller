<?php

require_once('neural-network/class_neuralnetwork.php');

$noDefects = 11;

$properties = ['category', 'brand', 'sfm', 'sab'];

$input = [];

foreach ($properties as $property) {
    if (!isset($_POST[$property])) {
        return json_encode(['status' => 'error', 'message' => $property. ' not received!']);
    }

    $input[] = $_POST[$property];
}

for ($i=1; $i<=$noDefects; $i++) {
    if (!isset($_POST['defect'.$i])) {
        return json_encode(['status' => 'error', 'message' => 'Defect '. $i. ' not received!']);
    }

    $input[] = $_POST['defect'.$i];
}

try {
    $neuralNetwork = new NeuralNetwork([count($input), 5, 5, 3]);
    $neuralNetwork->setVerbose(false);
    //$neuralNetwork->load('my_network.ini');

    $result = $neuralNetwork->calculate($input);
} catch (\Exception $e) {
    return json_encode(['status' => 'error', 'message' => $e->getMessage()]);
}

return json_encode(['status' => 'success', 'result' => $result]);
